package keystore;

import java.util.Arrays;

public class KeyStore01 {
	//Attribute
	private String[] stringi;
	private int einschreiben = -1;
	
	//Konstruktor
	public KeyStore01() {
		stringi = new String[100];
	}
	
	public KeyStore01(int length) {
		stringi = new String[length];
	}
	
	//Methoden
	public boolean add(String e) {
		try {
			stringi[einschreiben + 1] = e;
		} catch (ArrayIndexOutOfBoundsException aioob){
			return false;
		}
		einschreiben++;
		return true;
	}
	
	public void clear() {
		stringi = new String[100];
		einschreiben = -1;
	}
	
	public String get(int index) {
		try {
			return stringi[index];
		} catch(ArrayIndexOutOfBoundsException aioob) {
			return "";
		}
	}
	
	public int indexOf(String str) {
		int NOT_GIVEN = -1;
		for (int i = 0; i < stringi.length; i++) {
			if (str.equals(stringi[i]))
				return i;
		}
		return NOT_GIVEN;
	}
	
	public boolean remove(int index) {
		try {
			stringi[index] = null;
		} catch(ArrayIndexOutOfBoundsException aioob) {
			return false;
		}
		return true;
	}
	
	public boolean remove(String str) {
		return remove(indexOf(str));
		
	}
	
	public int size() {
		return einschreiben + 1;
	}

	public String toString() {
		String ausgabe = "";
		for (int i = 0; i < stringi.length; i ++) {
			ausgabe += stringi[i] + ", ";
		}
		return ausgabe;
	}
	
	
}
