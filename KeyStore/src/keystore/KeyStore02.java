package keystore;

import java.util.ArrayList;

public class KeyStore02 {
	//Attribute
		private ArrayList<String> stringi;
		
		//Konstruktor
		public KeyStore02() {
			stringi = new ArrayList<String>(100);
		}
		
		public KeyStore02(int length) {
			stringi = new ArrayList<String>(length);
		}
		
		//Methoden
		public boolean add(String e) {
			return stringi.add(e);
		}
		
		public void clear() {
			stringi.clear();
		}
		
		public String get(int index) {
			return stringi.get(index);
		}
		
		public int indexOf(String str) {
			return stringi.indexOf(str);
		}
		
		public boolean remove(int index) {
			String temp = stringi.get(index);
			if (stringi.remove(index).equals(temp)) {
				return true;
			}	
			return false;
		}
		
		public boolean remove(String str) {
			return stringi.remove(str);
			
		}
		
		public int size() {
			return stringi.size();
		}

		public String toString() {
			return stringi.toString();
		}
}
