package oszKickers;

public class Spieler extends Mitglied{
	private int trikotnummer;
	private String spielposition;
	
	public Spieler() {
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}
	
}
