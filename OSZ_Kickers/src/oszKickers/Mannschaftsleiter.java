package oszKickers;

public class Mannschaftsleiter extends Spieler{
	private String nameMannschaft;
	private String zusatsAufgaben;
	private int rabatt;
	
	public Mannschaftsleiter() {
	}

	public String getNameMannschaft() {
		return nameMannschaft;
	}

	public void setNameMannschaft(String nameMannschaft) {
		this.nameMannschaft = nameMannschaft;
	}

	public String getZusatsAufgaben() {
		return zusatsAufgaben;
	}

	public void setZusatsAufgaben(String zusatsAufgaben) {
		this.zusatsAufgaben = zusatsAufgaben;
	}

	public int getRabatt() {
		return rabatt;
	}

	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
	
}
