package oszKickers;

public class Schiedsrichter extends Mitglied {
	private int gepfiffeneSpiele;

	public Schiedsrichter() {
	}

	public int getGepfiffeneSpiele() {
		return gepfiffeneSpiele;
	}

	public void setGepfiffeneSpiele(int gepfiffeneSpiele) {
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}
	
}
