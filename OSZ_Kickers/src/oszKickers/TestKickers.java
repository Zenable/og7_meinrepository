package oszKickers;

public class TestKickers {

	public static void main(String[] args) {
		
		String name = "Peter";
		int nummer = 69;
		String position = "Sturm";
		long tnummer = 808666666;
		boolean hatBezahlt = false;
		String mName = "OSZ Kickers TORrnado 1996";
		int rabatt = -4;
		String zusatzAufgabe = "Toiletten putzen";
		
		//Spieler Test		
		Spieler spieler = new Spieler();
		spieler.setTrikotnummer(nummer);
		spieler.setSpielposition(position);
		spieler.setName(name);
		spieler.setTelefonnummer(tnummer);
		spieler.setHatBezahlt(hatBezahlt);
		System.out.println(spieler + " " + spieler.getName() + " " + spieler.getTelefonnummer() +  " " + spieler.getHatBezahlt());
	
		//Mannschaftsleiter Test
		Mannschaftsleiter mannschaftsleiter = new Mannschaftsleiter();
		mannschaftsleiter.setName(name);
		mannschaftsleiter.setHatBezahlt(hatBezahlt);
		mannschaftsleiter.setTelefonnummer(tnummer);
		mannschaftsleiter.setSpielposition(position);
		mannschaftsleiter.setTrikotnummer(nummer);
		mannschaftsleiter.setNameMannschaft(mName);
		mannschaftsleiter.setZusatzAufgaben(zusatzAufgabe);
		mannschaftsleiter.setRabatt(rabatt);
		System.out.println(mannschaftsleiter + " " + mannschaftsleiter.getName() + " " + mannschaftsleiter.getTelefonnummer()
		+ " " + mannschaftsleiter.getHatBezahlt()+ " " + mannschaftsleiter.getTrikotnummer() + " " + mannschaftsleiter.getSpielposition());
		
		//Schiedsrichter
		Schiedsrichter schiedsrichter = new Schiedsrichter();
		schiedsrichter.setGepfiffeneSpiele(132);
		schiedsrichter.setName("Gunther");
		schiedsrichter.setHatBezahlt(true);
		schiedsrichter.setTelefonnummer(12344221);
		System.out.println(schiedsrichter + " " + schiedsrichter.getName() + " " + schiedsrichter.getTelefonnummer() + " " + schiedsrichter.getHatBezahlt());
		
		//Trainer
		Trainer trainer = new Trainer();
		trainer.setEntschaedigung(100);
		trainer.setLizenzklasse('A');
		trainer.setSpielklasse("B Jugend");
		trainer.setName("Manny");
		trainer.setHatBezahlt(true);
		trainer.setTelefonnummer(123343212);
		System.out.println(trainer + " " + trainer.getName() + " " + trainer.getTelefonnummer() + " " + trainer.getHatBezahlt());
	}

}
