package oszKickers;

public class Trainer extends Mitglied{
	private char lizenzklasse;
	private int entschaedigung;
	private String spielklasse;
	
	public Trainer() {
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public int getEntschaedigung() {
		return entschaedigung;
	}

	public void setEntschaedigung(int entschaedigung) {
		this.entschaedigung = entschaedigung;
	}

	public String getSpielklasse() {
		return spielklasse;
	}

	public void setSpielklasse(String spielklasse) {
		this.spielklasse = spielklasse;
	}
	
}
