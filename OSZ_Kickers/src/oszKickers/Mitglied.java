package oszKickers;

public abstract class Mitglied {
	private String name;
	private long telefonnummer;
	private boolean hatBezahlt;
	
	public Mitglied() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(long telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean isHatBezahlt() {
		return hatBezahlt;
	}

	public void setHatBezahlt(boolean hatBezahlt) {
		this.hatBezahlt = hatBezahlt;
	}
	
}
