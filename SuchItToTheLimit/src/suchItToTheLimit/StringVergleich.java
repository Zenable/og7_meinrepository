package suchItToTheLimit;

import java.util.List;
import java.util.ArrayList;


public class StringVergleich {
	
	public static void main(String[] args) {
		ArrayList<String> listi = new ArrayList<String>();
		listi.add("Lurchi");
		listi.add("Murchi");
		listi.add("Kurchi");
		listi.add("Rurchi");
		System.out.println(listi);
		System.out.println(suchenBisZumFinden(listi,"Murchi"));
		System.out.println(sortierenBisZumAnfang(listi));
	}
	
	public static int suchenBisZumFinden(List<String> listi, String gesuchtBrudaaa) {
		for (int i = 0; i < listi.size(); i++)
			if(gesuchtBrudaaa.equals(listi.get(i)))
				return i;
		return -1;
	}
	
	public static List<String> sortierenBisZumAnfang(List<String> listi) {
		for (int i = 0; i < listi.size(); i++) {
			for (int n = listi.size() - 1; n > i; n--) {
				if (listi.get(i).compareTo(listi.get(n)) > 0) {
					
					String temp = listi.get(i);
					listi.set(i, listi.get(n));
					listi.set(n, temp);
				}
			}
		}
		return listi;
	}
}
