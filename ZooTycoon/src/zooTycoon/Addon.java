package zooTycoon;

public class Addon {
	private int id;
	private String bezeichnung;
	private double preis;
	private int anzahlBestand;
	private int maxBestand;
	
	public Addon(int id, String bezeichnung, double preis, int anzahlBestand, int maxBestand) {
		super();
		this.id = id;
		this.bezeichnung = bezeichnung;
		this.preis = preis;
		this.anzahlBestand = anzahlBestand;
		this.maxBestand = maxBestand;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getAnzahlBestand() {
		return anzahlBestand;
	}

	public void setAnzahlBestand(int anzahlBestand) {
		this.anzahlBestand = anzahlBestand;
	}

	public int getMaxBestand() {
		return maxBestand;
	}

	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}
	
	public double getGesamtwert() {
		return this.preis * this.anzahlBestand;
	}

	@Override
	public String toString() {
		return "Addon [getId()=" + getId() + ", getBezeichnung()=" + getBezeichnung() + ", getPreis()=" + getPreis()
				+ ", getAnzahlBestand()=" + getAnzahlBestand() + ", getMaxBestand()=" + getMaxBestand()
				+ ", getGesamtwert()=" + getGesamtwert() + "]";
	}
	
}
