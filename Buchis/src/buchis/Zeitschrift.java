package buchis;

public class Zeitschrift extends Medium{
	private String issn;
	private int jahrgang;
	private long nummer;
	
	public Zeitschrift(String ean, String titel, String issn, int jahrgang, long nummer) {
		this.ean = ean;
		this.titel = titel;
		this.issn = issn;
		this.jahrgang = jahrgang;
		this.nummer = nummer;
	}

	public String getIssn() {
		return issn;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public int getJahrgang() {
		return jahrgang;
	}

	public void setJahrgang(int jahrgang) {
		this.jahrgang = jahrgang;
	}

	public long getNummer() {
		return nummer;
	}

	public void setNummer(long nummer) {
		this.nummer = nummer;
	}

	@Override
	public String toString() {
		return "\t EAN: " + this.ean + "\n\t Titel: " + this.titel + "\n\t ISSN: " + this.issn+ 
				"\n\t Jahrgang: " + this.jahrgang + "\n\t Nummer: " + this.nummer;
	}
	
	
	
}
