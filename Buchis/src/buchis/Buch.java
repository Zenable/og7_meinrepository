package buchis;

public class Buch extends Medium implements Comparable<Buch>{
	private String autor;
	private String titel;
	private String isbn;
	
	public Buch(String autor, String titel, String isbn) {
		this.autor = autor;
		this.titel = titel;
		this.isbn = isbn;
	}
	
	public Buch(String ean, String autor, String titel, String isbn) {
		this.ean = ean;
		this.autor = autor;
		this.titel = titel;
		this.isbn = isbn;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	
	@Override
	public String toString() {
		return "\n\t EAN: " + this.ean + "\n\t Autor: " + this.autor + "\n\t Titel: " + 
				this.titel + "\n\t ISBN: " + this.isbn;
	}

	@Override
	public boolean equals(Object o) {
		return ((Buch) o).getIsbn().equals(this.isbn);
	}

	@Override
	public int compareTo(Buch aBuch) {
		return this.isbn.compareTo(aBuch.getIsbn());
	}

}