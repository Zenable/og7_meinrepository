package buchis;

public class DVD extends Film{

	public DVD(String ean, String titel,int laufzeit) {
		this.ean = ean;
		this.titel = titel;
		this.laufzeit = laufzeit;
	}

	@Override
	public String toString() {
		return "\n\t EAN: " + this.ean + "\n\t Titel: " + this.titel + "\n\t Laufzeit: " + this.laufzeit;
	}
	
	
}
