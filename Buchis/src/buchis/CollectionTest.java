package buchis;

import java.util.*;

public class CollectionTest {
	
	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die gr��te ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Buch> buchliste = new LinkedList<Buch>();
        Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;
		
		int index;
		do {
			menue();
			wahl = myScanner.nextLine().charAt(0);
			switch (wahl) {
			case '1':
				System.out.print("Bitte geben Sie den Buchtitel ein:\t");
				String titel = myScanner.nextLine();
				System.out.print("\nBitte geben Sie den Autor ein:\t");
				String autor = myScanner.nextLine();
				System.out.print("\nBitte geben Sie die ISBN ein:\t");
				String isbn = myScanner.nextLine();
				Buch buch = new Buch(autor, titel, isbn);
                System.out.println(buchliste.add(buch));
				break;
			case '2':
				System.out.print("Bitte geben Sie die ISBN ein um das Buch zu finden:\t");
				eintrag = myScanner.nextLine();
				System.out.println(findeBuch(buchliste, eintrag));
				break;
			case '3':
				System.out.print("Bitte geben Sie die ISBN ein um das Buch zu löschen:\t");
				eintrag = myScanner.nextLine();
				System.out.println(loescheBuch(buchliste, eintrag));
				break;
			case '4':
				System.out.println("Die größte ISBN ist:\t" + ermitteleGroessteISBN(buchliste));
				break;		
			case '5':
				System.out.println(buchliste);
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
		} // switch

	} while (wahl != 9);
}// main

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		for (int i = 0; i < buchliste.size(); i++) {
			if (isbn.equals(buchliste.get(i).getIsbn()))
			return buchliste.get(i);	
		}
		return null;
	}

	public static boolean loescheBuch(List<Buch> buchliste, String isbn) {
		if (findeBuch(buchliste, isbn) != null) {
			buchliste.remove(findeBuch(buchliste, isbn));
			return true;
		}
		return false;	
	}

	public static String ermitteleGroessteISBN(List<Buch> buchliste) {
		 String isbn = "1";
		 for (int i = 0; i < buchliste.size(); i++)
			 if (buchliste.get(i).getIsbn().compareTo(isbn) > 0) 
				 isbn = buchliste.get(i).getIsbn();
		 return isbn;
	}
}
