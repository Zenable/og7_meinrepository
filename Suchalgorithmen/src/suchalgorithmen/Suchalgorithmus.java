package suchalgorithmen;

import java.util.Random;
import java.util.Scanner;

public class Suchalgorithmus {
	/*
	 * Stoppuhr
	 */
	private static long start;
	private static long stop;
	
	public static void start() {
		start = System.currentTimeMillis();
	}
	
	public static void stop() {
		stop = System.currentTimeMillis();
	}
	
	public static long getTime() {
		return stop - start;
	}
	
	/*
	 * geordnette Liste
	 */
	public static long[] getGeordneteListe(int laenge) {
		Random rand = new Random(laenge);
		long[] zahlenliste = new long[laenge];
		long naechsteZahl = 0;
		
		for(int i = 1; i < laenge; i++){
			naechsteZahl += rand.nextInt(1) + 1;
			zahlenliste[i] = naechsteZahl;
		}
		
		return zahlenliste;
	}
	
	/*
	 * linearer Suchalgorithmus
	 */
	public static long linearSuchi(long zahl, int laenge, long[] listi) {
		final int NICHT_GEFUNDEN = -1;
		for (int i = 0; i < listi.length; i++) {
			if (listi[i] == zahl)
				return zahl;
		}
		return NICHT_GEFUNDEN;
	}
	
	/*
	 * Fastsearch Suchalgorithmus
	 */
	public static int fasti(int von, int bis, long zahl, long[] listi) {
		final int NICHT_GEFUNDEN = -1;
		if (von <= bis) {
			int mB = Math.abs((von + bis) / 2);
			int mI = (int) (von + Math.abs((bis - von) * ((zahl - listi[von]) / (listi[bis] - listi[von]))));
			if (mI > listi.length)
				return NICHT_GEFUNDEN;
			if (mB > mI) {
				int temp = mB;
				mB = mI;
				mI = temp;
			}
			if (zahl == listi[mB]) {
				return mB;
			} else if (zahl == listi[mI]){
				return mI;
			} else if (zahl < listi[mB]) {
				return fasti(von, mB - 1, zahl, listi);
			} else if (zahl < listi[mI]) {
				return fasti(mB + 1, mI - 1, zahl, listi);
			} else {
				return fasti(mI + 1, bis, zahl, listi);
			}
			
		}
		return NICHT_GEFUNDEN;
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Geben Sie die L�nge einer Liste ein:\t");
		int laenge = scan.nextInt();
		System.out.print("Geben Sie bitte eine zu findene Zahl ein:\t");
		long zahl = scan.nextLong();
		System.out.print("Geben Sie die Anzahl der Durchl�ufe ein:\t");
		int eingabe = scan.nextInt();
		scan.close();
		long[] listi = getGeordneteListe(laenge);
		//start der linearen Suche
		System.out.println("lineare Suche: ");
		for (int i = 0; i < eingabe; i++) {
			start();
			System.out.println(linearSuchi(zahl, laenge, listi));
			stop();		
			System.out.println(getTime() + "ms");
		}
		// start des Fastsearch
		System.out.println("Fastsearch: ");
		for (int i = 0; i < eingabe; i++) {
			start();
			System.out.println(fasti(0, laenge - 1, zahl, listi));
			stop();
			System.out.println(getTime() + "ms");
		}
					
	}
	
	
}

