package omnom;

public class Haustier {
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private String name;
	
	
	public Haustier() {
		this.setHunger(100);
		this.setGesund(100);
		this.setMuede(100);
		this.setZufrieden(100);
	}
	
	public Haustier(String name) {
		this.name = name;
		this.setHunger(100);
		this.setGesund(100);
		this.setMuede(100);
		this.setZufrieden(100);
	}

	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {	
		if(hunger < 0) 
			hunger = 0;
		if(hunger > 100) 
			hunger = 100;
		this.hunger = hunger;		
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		if(muede < 0) 
			muede = 0;
		if(muede > 100) 
			muede = 100;
		this.muede = muede;
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if(zufrieden < 0) 
			zufrieden = 0;
		if(zufrieden > 100) 
			zufrieden = 100;
		this.zufrieden = zufrieden;
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {	
		this.gesund = 100;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void schlafen(int dauer) {
		this.setMuede(this.getMuede() + 20);
	}
	
	public void fuettern(int anzahl) {
		this.setHunger(this.getHunger() + 40);
	}
	
	public void spielen(int dauer) {
		this.setZufrieden(this.getZufrieden() + 10);
	}
	
	public void heilen() {
		this.setGesund(this.getGesund());
	}
}

