package felder;
/**
  *
  * Übungsklasse zu Feldern
  *
  * @version 1.0 vom 05.05.2011
  * @author Tenbusch
  */
import java.util.Arrays;
public class Felder {

  //unsere Zahlenliste zum Ausprobieren
  private int[] zahlenliste = {5,8,4,3,9,1,2,7,6,0};
  
  //Konstruktor
  public Felder(){}

  //Methode die Sie implementieren sollen
  //ersetzen Sie den Befehl return 0 durch return ihre_Variable
  
  //die Methode soll die größte Zahl der Liste zurückgeben
  public int maxElement(){
	  int max = zahlenliste[0];
	  for(int i = 0; i < zahlenliste.length; i++) {
		 if (max < zahlenliste[i])
			 max = zahlenliste[i];
	  }
	  return max;
  }

  //die Methode soll die kleinste Zahl der Liste zurückgeben
  public int minElement(){
	  int min = zahlenliste[0];
	  for(int i = 0; i < zahlenliste.length; i++) {
		  if(min > zahlenliste[i])
			  min = zahlenliste[i];
	  }
	  return min;
  }
  
  //die Methode soll den abgerundeten Durchschnitt aller Zahlen zurückgeben
  public int durchschnitt(){
	  int durch = 0;
	  for(int i = 0; i < zahlenliste.length; i++) {
		  durch += zahlenliste[i];
	  }
	  return durch / zahlenliste.length;
  }

  //die Methode soll die Anzahl der Elemente zurückgeben
  //der Befehl zahlenliste.length; könnte hierbei hilfreich sein
  public int anzahlElemente(){
    return zahlenliste.length;
  }

  //die Methode soll die Liste ausgeben
  public String toString(){
	  String liste = "";
	  for(int i = 0; i < zahlenliste.length; i++) {
		  liste += zahlenliste[i] + ", ";
	  }
	  return liste;
  }

  //die Methode soll einen booleschen Wert zurückgeben, ob der Parameter in
  //dem Feld vorhanden ist
  public boolean istElement(int zahl){
	  boolean istElement = false;
	  for(int i = 0; i < zahlenliste.length; i++) {
		  if(zahl == zahlenliste[i])
			  istElement = true;
	  }
	  return istElement;
  }
  
  //die Methode soll das erste Vorkommen der
  //als Parameter übergebenen  Zahl liefern oder -1 bei nicht vorhanden
  public int getErstePosition(int zahl){
	  int posi = -1;
	  for(int i = 0; i < zahlenliste.length; i++) {
		  if(zahl == zahlenliste[i])
			  return i;
	  }
	  return posi;
  }
  
  //die Methode soll die Liste aufsteigend sortieren
  //googlen sie mal nach Array.sort() ;)
  public void sortiere(){
	 Arrays.sort(zahlenliste); 
  }

  public static void main(String[] args) {
    Felder testenMeinerLösung = new Felder();
    System.out.println(testenMeinerLösung.maxElement());
    System.out.println(testenMeinerLösung.minElement());
    System.out.println(testenMeinerLösung.durchschnitt());
    System.out.println(testenMeinerLösung.anzahlElemente());
    System.out.println(testenMeinerLösung.toString());
    System.out.println(testenMeinerLösung.istElement(9));
    System.out.println(testenMeinerLösung.getErstePosition(5));
    testenMeinerLösung.sortiere();
    System.out.println(testenMeinerLösung.toString());
  }
}
