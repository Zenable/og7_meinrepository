package layouts;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import java.awt.CardLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.BoxLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;
import javax.swing.JLabel;

public class Cardierichtig extends JFrame {

	JPanel cards;
	CardLayout cardL;
	final static String TEST = "Erstes Panel";
	final static String WIESO = "WIESO?";
	

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cardierichtig frame = new Cardierichtig();
					frame.setVisible(true);
					frame.pack();
					frame.cardL.show(frame.cards, TEST);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Cardierichtig() {
		cardL = new CardLayout();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		cards = new JPanel();
		cards.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(cards);
		cards.setLayout(cardL);
		
		JPanel pnl_1 = new JPanel();
		pnl_1.setBackground(Color.YELLOW);
		cards.add(pnl_1, TEST);
		
		JButton btnSwitch = new JButton("Switch");
		btnSwitch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cardL.show(cards, WIESO);
			}
		});
		pnl_1.setLayout(new GridLayout(2, 1, 0, 0));
		pnl_1.add(btnSwitch);
		
		JPanel pnl_2 = new JPanel();
		pnl_2.setBackground(Color.RED);
		cards.add(pnl_2, WIESO);
		pnl_2.setLayout(new GridLayout(2, 1, 0, 0));
		
		JButton btnSwitch_2 = new JButton("Switch");
		btnSwitch_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cardL.show(cards, TEST);
			}
		});
		pnl_2.add(btnSwitch_2);
	}

}
