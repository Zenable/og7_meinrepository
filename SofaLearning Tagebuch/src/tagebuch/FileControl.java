package tagebuch;

import java.util.List;
import java.util.ArrayList;
import java.io.*;


public class FileControl {
	
	private String dateiName;
	
	public FileControl(String dateiName) {
		this.dateiName = dateiName;
	}
	
	public List<String> dateiEinlesen() {
		
		File file = new File(dateiName);
		List<String> zeilen = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(file))){
			String s = "";
			while ((s = br.readLine()) != null) {
				zeilen.add(s);
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return zeilen;
		
	}
	
}
