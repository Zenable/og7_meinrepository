package tagebuch;

import java.util.ArrayList;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class Logic {
	
	private FileControl file;
	private List<Lerneintrag> lernen;
	private int gesDauer;
	private int durchDauer;
	
	public Logic(FileControl fileControl) {
		this.file = fileControl;
		reloadFromDisk();
	}
	
	public void reloadFromDisk() {
		StringtoLerneintrag();
	}
	
	public List<Lerneintrag> getListEntries(){
		return this.lernen;
	}
	
	public String getLearnerName() {
		return file.dateiEinlesen().get(0);
	}
	
	public void StringtoLerneintrag() {
		lernen = new ArrayList<>();
		for (int i = 1; i < file.dateiEinlesen().size(); i += 4) {
			SimpleDateFormat meinDatumsformat = new SimpleDateFormat("dd.mm.yyyy");
			Date datum = new Date();
			try {datum = meinDatumsformat.parse(file.dateiEinlesen().get(i));
			} catch(ParseException pe) {
				pe.printStackTrace();
			}
			
			int dauer = Integer.parseInt(file.dateiEinlesen().get(i + 3));
			
			this.lernen.add(new Lerneintrag(datum, file.dateiEinlesen().get(i + 1), file.dateiEinlesen().get(i + 2), dauer));
		}
		
	}
	
	public String createReport() {
		for(int i = 0; i < lernen.size(); i++) {
			this.gesDauer += lernen.get(i).getDauer();
		}
		durchDauer = gesDauer / lernen.size();
		return durchDauer + " " + gesDauer;
	}
	
}
