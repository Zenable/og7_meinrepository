package tagebuch;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Lerneintrag {
	private Date datum;
	private String fach;
	private String beschreibung;
	private int dauer;
	
	public Lerneintrag(Date datum2, String fach, String beschreibung, int dauer) {
		this.datum = datum2;
		this.fach = fach;
		this.beschreibung = beschreibung;
		this.dauer = dauer;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public String getFach() {
		return fach;
	}

	public void setFach(String fach) {
		this.fach = fach;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public int getDauer() {
		return dauer;
	}

	public void setDauer(int dauer) {
		this.dauer = dauer;
	}

	@Override
	public String toString() {
		SimpleDateFormat meinDati = new SimpleDateFormat("dd.mm.yyyy");
		
		String datum = String.format("%-7s", meinDati.format(this.getDatum()));
		String fach = String.format(" " + "%-10s", this.getFach().length() > 8 ? this.getFach().substring(0, 7) : this.getFach());
		String aktivitaet = String.format(" " + "%-42s", this.getBeschreibung().length() > 41 ? this.getBeschreibung().substring(0, 34) : this.getBeschreibung());
		String dauer = String.format(" " + "%-7s", (this.getDauer() + "").length() > 6 ? (this.getDauer() + "").substring(0, 2) : this.getDauer() + "");
		
		return datum + "|" + fach + "|" + aktivitaet + "|" + dauer;
	}
	
	
}
