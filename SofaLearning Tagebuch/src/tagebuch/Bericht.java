package tagebuch;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

public class Bericht extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public Bericht(TagebuchhGui gui) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		String men = gui.logisch.createReport();
		String[] berichten  = men.split(" ");
		
		JLabel lbl = new JLabel("");
		lbl.setBounds(0, 0, 210, 261);
		contentPane.add(lbl);
		lbl.setText("Gesamtdauer ");
		lbl.setText(lbl.getText() + berichten[1]);
		
		JLabel label = new JLabel("");
		label.setBounds(220, 0, 210, 261);
		contentPane.add(label);
		label.setText("Durchschnittsdauer ");
		label.setText(label.getText() + berichten[0]);
	}
}
