package tagebuch;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.BoxLayout;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.FlowLayout;
import java.awt.CardLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.UIManager;
import java.awt.Font;
import javax.swing.JScrollBar;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class TagebuchhGui extends JFrame {

	private JPanel contentPane;
	public Logic logisch;
	private TagebuchhGui gui;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Logic logisch = new Logic(new FileControl("miriam.dat"));
		TagebuchhGui frame = new TagebuchhGui(logisch);
		frame.setVisible(true);
			
	}

	/**
	 * Create the frame.
	 */
	public TagebuchhGui(Logic logisch) {
		this.logisch = logisch;
		this.gui = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 652, 386);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNeuerEintrag = new JButton("Neuer Eintrag");
		btnNeuerEintrag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNeuerEintrag.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		btnNeuerEintrag.setBounds(230, 307, 139, 29);
		contentPane.add(btnNeuerEintrag);
		
		JButton btnBericht = new JButton("Bericht...");
		btnBericht.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				(new Bericht(gui)).show(true);
			}
		});
		btnBericht.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		btnBericht.setBounds(379, 307, 98, 29);
		contentPane.add(btnBericht);
		
		JButton btnBeenden = new JButton("Beenden");
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnBeenden.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
		btnBeenden.setBounds(487, 307, 139, 29);
		contentPane.add(btnBeenden);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(609, 93, 17, 203);
		contentPane.add(scrollBar);
		
		JLabel lblLerneintrag = new JLabel("Lerntagebuch von " + logisch.getLearnerName());
		lblLerneintrag.setFont(new Font("Comic Sans MS", Font.BOLD, 26));
		lblLerneintrag.setHorizontalAlignment(SwingConstants.LEFT);
		lblLerneintrag.setBounds(10, 11, 589, 63);
		contentPane.add(lblLerneintrag);
		
		JTextArea textArea = new JTextArea();
		textArea.setFont(new Font("Consolas", Font.PLAIN, 13));
		textArea.setBounds(10, 93, 589, 203);
		contentPane.add(textArea);
		
		textArea.setText("Datum     | Fach      | Aktivitšt                                 | Dauer  ");
		textArea.setText(textArea.getText() + "\n--------------------------------------------------------------------------\n");
		for(int i = 0; i < logisch.getListEntries().size(); i++)
			textArea.setText(textArea.getText() + logisch.getListEntries().get(i).toString() + "\n");
	}
}
